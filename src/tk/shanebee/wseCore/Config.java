package tk.shanebee.wseCore;

import org.bukkit.Location;
import org.bukkit.configuration.Configuration;

import java.io.File;

public class Config {

	private WSECore plugin;

	private Configuration config;
	public Location spawnLocation;
	public int rtpMax;
	public int rtpWait;

	Config(WSECore plugin) {
		this.plugin = plugin;
		if (!new File(plugin.getDataFolder(), "config.yml").exists()) {
			plugin.saveDefaultConfig();
		}
		config = plugin.getConfig();

		this.rtpMax = loadValue("rtp.max", 2500);
		this.rtpWait = loadValue("rtp.wait", 60);
		config.options().copyDefaults(true);
		plugin.saveConfig();

		spawnLocation = config.getObject("spawn.location", Location.class);
	}

	public void saveValue(String value, Object object) {
		this.plugin.getConfig().set(value, object);
		this.plugin.saveConfig();
	}

	private Object loadValue(String path, Object value) {
		config.addDefault(path, value);
		return config.get(path);
	}

	private int loadValue(String path, int value) {
		config.addDefault(path, value);
		return config.getInt(path);
	}

}
