package tk.shanebee.wseCore.utils;

import org.bukkit.World;

public enum Time {
	SUNRISE("Sunrise"),
	MORNING("Morning"),
	NOON("Noon"),
	AFTERNOON("Afternoon"),
	EVENING("Evening"),
	SUNSET("Sunset"),
	NIGHT("Night"),
	LATE_NIGHT("Late Night"),
	MIDNIGHT("Midnight");

	String name;

	Time(String name) {
		this.name = name;
	}

	public static Time getTime(World world) {
		long time = world.getTime() % 24000;
		if (isBetween(time, 0, 450))
			return SUNRISE;
		else if (isBetween(time, 451, 5999))
			return MORNING;
		else if (isBetween(time, 6000, 7700))
			return NOON;
		else if (isBetween(time, 7701, 9000))
			return AFTERNOON;
		else if (isBetween(time, 9001, 11616))
			return EVENING;
		else if (isBetween(time, 11617, 13799))
			return SUNSET;
		else if (isBetween(time, 13800, 15999))
			return NIGHT;
		else if (isBetween(time, 16000, 18000))
			return LATE_NIGHT;
		else if (isBetween(time, 18001, 22550))
			return MIDNIGHT;
		else if (isBetween(time, 22551, 24000))
			return SUNRISE;
		else
			return SUNRISE;
	}

	public static String getTimeName(World world) {
		long time = world.getTime() % 24000;
		if (isBetween(time, 0, 450))
			return SUNRISE.name;
		else if (isBetween(time, 451, 5999))
			return MORNING.name;
		else if (isBetween(time, 6000, 7700))
			return NOON.name;
		else if (isBetween(time, 7701, 9000))
			return AFTERNOON.name;
		else if (isBetween(time, 9001, 11616))
			return EVENING.name;
		else if (isBetween(time, 11617, 13799))
			return SUNSET.name;
		else if (isBetween(time, 13800, 15999))
			return NIGHT.name;
		else if (isBetween(time, 16000, 18000))
			return LATE_NIGHT.name;
		else if (isBetween(time, 18001, 22550))
			return MIDNIGHT.name;
		else if (isBetween(time, 22551, 24000))
			return SUNRISE.name;
		else
			return SUNRISE.name;
	}

	private static boolean isBetween(long check, long b1, long b2) {
		return check >= b1 && check <= b2;
	}


}
