package tk.shanebee.wseCore.utils;

import org.bukkit.World;

public enum Weather {
	SUNNY,
	RAINY,
	STORMY;

	public static Weather getWeather(World world) {
		if (world.hasStorm() && world.isThundering()) {
			return STORMY;
		} else if (world.hasStorm() && !world.isThundering()) {
			return RAINY;
		} else {
			return SUNNY;
		}
	}

	public static void setWeather(World world, Weather weather) {
		switch (weather) {
			case SUNNY:
				world.setStorm(false);
				world.setThundering(false);
				break;
			case RAINY:
				world.setStorm(true);
				world.setThundering(false);
				break;
			case STORMY:
				world.setStorm(true);
				world.setThundering(true);
		}
	}
}
