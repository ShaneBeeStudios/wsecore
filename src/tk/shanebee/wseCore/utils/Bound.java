package tk.shanebee.wseCore.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;

public class Bound {

	private int x;
	private int y;
	private int z;
	private int x2;
	private int y2;
	private int z2;
	private String world;

	public Bound(String world, int x, int y, int z, int x2, int y2, int z2) {
		this.world = world;
		this.x = Math.min(x,x2);
		this.y = Math.min(y, y2);
		this.z = Math.min(z, z2);
		this.x2 = Math.max(x,x2);
		this.y2 = Math.max(y, y2);
		this.z2 = Math.max(z, z2);
	}

	public ArrayList<Location> getBlockLocations(Material type) {
		World w = Bukkit.getWorld(world);
		ArrayList <Location> array = new ArrayList<>();
		for (int x3 = x; x3 <= x2; x3++) {
			for (int y3 = y; y3 <= y2; y3++) {
				for (int z3 = z; z3 <= z2; z3++) {
					assert w != null;
					Block b = w.getBlockAt(x3, y3, z3);
					if (b.getType() == type) {
						array.add(b.getLocation());
					}
				}
			}
		}
		return array;
	}

	public ArrayList<Block> getBlocks(Material... type) {
		World w = Bukkit.getWorld(world);
		ArrayList <Block> array = new ArrayList<>();
		for (int x3 = x; x3 <= x2; x3++) {
			for (int y3 = y; y3 <= y2; y3++) {
				for (int z3 = z; z3 <= z2; z3++) {
					assert w != null;
					Block b = w.getBlockAt(x3, y3, z3);
					if (Arrays.asList(type).contains(b.getType())) {
						array.add(b);
					}
				}
			}
		}
		return array;
	}

}
