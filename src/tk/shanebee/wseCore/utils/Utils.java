package tk.shanebee.wseCore.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Utils {

	public static String colString(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	public static void sendColConsole(String message) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	public static void log(String message) {
		String prefix = "&7[&bW&7-&3S&7-&bE&7] &r";
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + message));
	}

	public static void warn(String message) {
		String prefix = "&7[&bW&7-&3S&7-&bE&7] &e";
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + message));
	}

	public static void inGameOnlyCommand() {
		warn("&cThis command can only be run in game!");
	}

	public static void scm(Player player, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	public static void sendPluginMessage(Player player, String message) {
		String prefix = "&7[&bW&7-&3S&7-&bE&7] - &r";
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + message));
	}
}
