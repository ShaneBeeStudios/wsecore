package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.util.Random;

public class SpawnCmd implements CommandExecutor {

	@SuppressWarnings("NullableProblems")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			Location spawn = WSECore.plugin.getWSEConfig().spawnLocation;
			if (player.hasPermission("wse.bypass")) {
				player.teleport(spawn);
				Utils.sendPluginMessage(player, "&6You have been teleported to spawn");
			} else {
				int random = new Random().nextInt(100) + 1;
				if (random <= 90) {
					Utils.sendPluginMessage(player, "&6Teleporting to spawn... hold still");
					double x = player.getLocation().getX();
					double z = player.getLocation().getZ();
					Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
						if (player.getLocation().getX() == x && player.getLocation().getZ() == z) {
							player.teleport(spawn);
							Utils.sendPluginMessage(player, "&6You have been teleported to spawn");
							player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1, 1);
						} else {
							Utils.sendPluginMessage(player, "&cOOPS ¯\\_(ツ)_/¯... looks like you moved a little");
							player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1, 1);
						}
					}, 60);
				} else {
					Utils.sendPluginMessage(player, "&cOOPS ¯\\_(ツ)_/¯... looks like we had a little problem");
				}
			}
		} else {
			Utils.inGameOnlyCommand();
		}
		return true;
	}

}
