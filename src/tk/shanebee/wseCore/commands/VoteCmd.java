package tk.shanebee.wseCore.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.data.PlayerData.PlayerDataType;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SuppressWarnings("NullableProblems")
public class VoteCmd implements CommandExecutor {

	public VoteCmd() {
		voteMessages();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			if (cmd.getName().equalsIgnoreCase("vote")) {
				sendVoteMessage(player);
			} else if (cmd.getName().equalsIgnoreCase("votekey")) {

				if (canVote(player)) {
					Utils.scm(player, "&7[&bVOTES&7] - &aThanks for voting!");
					voteKey(player);
					saveVote(player);
				} else {
					Utils.scm(player, "&7[&bVOTES&7] - &cYou already voted today!");
				}
			}

		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run in game!");
		}
		return true;
	}

	private void sendVoteMessage(Player player) {
		TextComponent message = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&7[&bVOTES&7] - "));
		TextComponent vote = new TextComponent(ChatColor.translateAlternateColorCodes('&',
				"&2https://www.planetminecraft.com/server/worst-server-ever/vote/"));
		vote.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to Vote!").create()));
		vote.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/votekey"));
		player.sendMessage(message, vote);
	}

	private void saveVote(Player player) {
		LocalDateTime date = LocalDateTime.now();
		String lastVote = "" + date.getYear() + "/" + date.getDayOfYear();
		WSECore.plugin.getPlayerData().savePlayerData(player, PlayerDataType.PLAYER_LAST_VOTE, lastVote);
		if (WSECore.plugin.getPlayerData().getPlayerData(player, PlayerDataType.PLAYER_TOTAL_VOTES) != null) {
			int total = Integer.valueOf(WSECore.plugin.getPlayerData().getPlayerData(player, PlayerDataType.PLAYER_TOTAL_VOTES).toString());
			WSECore.plugin.getPlayerData().savePlayerData(player, PlayerDataType.PLAYER_TOTAL_VOTES, total + 1);
		} else {
			WSECore.plugin.getPlayerData().savePlayerData(player, PlayerDataType.PLAYER_TOTAL_VOTES, 1);
		}
	}

	private boolean canVote(Player player) {
		if (WSECore.plugin.getPlayerData().getPlayerData(player, PlayerDataType.PLAYER_LAST_VOTE) != null) {
			String lastVote = WSECore.plugin.getPlayerData().getPlayerData(player, PlayerDataType.PLAYER_LAST_VOTE).toString();
			int year = Integer.valueOf(lastVote.split("/")[0]);
			int day = Integer.valueOf(lastVote.split("/")[1]);
			LocalDateTime date = LocalDateTime.now();
			return date.getYear() >= year && date.getDayOfYear() > day;
		}
		return true;
	}

	private void voteKey(Player player) {
		if (WSECore.plugin.getPerms().isInGroup(player, "VIP"))
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getName() + " VIP");
		else
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getName() + " vote");
	}

	private void voteMessages() {
		List<String> colors = Arrays.asList("&a", "&b", "&c", "&e", "&2", "&3", "&4", "&5", "&6", "&7");
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (canVote(player)) {
					Random random = new Random();
					for (int i = 0; i < 3; i++) {
						int ran = random.nextInt(10);
						player.sendMessage(Utils.colString("&7[&bVOTES&7] - " + colors.get(ran) + "Don't forget to vote today!"));
					}

				}
			}
		}, 20 * 60, 20 * 60);
	}

}
