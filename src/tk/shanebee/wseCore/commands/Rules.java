package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import tk.shanebee.wseCore.utils.Utils;

import java.util.ArrayList;

public class Rules implements Listener, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			openRulesGUI(player);
		} else {
			Utils.inGameOnlyCommand();
		}
		return true;
	}

	private ItemStack guiItem(Material material, String name, String... lore) {
		return guiItem(new ItemStack(material), name, lore);
	}

	private ItemStack guiItem(ItemStack stack, String name, String... lore) {
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(Utils.colString(name));
		ArrayList<String> loreList = new ArrayList<>();
		for (String l : lore) {
			loreList.add(Utils.colString(l));
		}
		loreList.add("");
		meta.setLore(loreList);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		stack.setItemMeta(meta);
		return stack;
	}

	private ItemStack playerHead(Player player) {
		ItemStack item = new ItemStack(Material.PLAYER_HEAD);
		SkullMeta skullMeta = ((SkullMeta) item.getItemMeta());
		skullMeta.setOwningPlayer(player);
		item.setItemMeta(skullMeta);
		return item;

	}

	private void openRulesGUI(Player player) {
		Inventory GUI = Bukkit.createInventory(player, 9, Utils.colString("&bServer &3Rules"));
		ArrayList<ItemStack> guiItems = new ArrayList<>();
		guiItems.add(guiItem(Material.PAPER, "&bCHAT", "&7Let's try keep the chat clean(ish)", "&7Swearing is &aallowed", "&7But try keep it minimal"));
		guiItems.add(guiItem(Material.DIAMOND_PICKAXE, "&2HACKS", "&7Hacking is &cNOT &7allowed", "&7Will result in ban", "&7We &aWILL &7find you... maybe &3¯\\_(ツ)_/¯"));
		guiItems.add(guiItem(Material.PLAYER_HEAD, "&3RESPECT", "&7Be respectful of others"));
		guiItems.add(guiItem(Material.MAP, "&9LAND&7-&9CLAIMS", "&7Respect other people's claims", "&7Do not claim within 100 blocks of another claim", "&7Unless you have their permission"));
		guiItems.add(guiItem(Material.IRON_SWORD, "&4PVP", "&7PVP is &aALLOWED", "&7Be careful out there"));
		guiItems.add(guiItem(Material.CHEST, "&5GRIEF", "&7We use &bGriefPrevetion &7to protect land", "&7Griefing outside claims is &aALLOWED", "&7Protect your stuff by claiming your land"));
		guiItems.add(guiItem(Material.GREEN_BED, "&eAKF", "&7Afk bypasses are &cNOT ALLOWED", "&7This will result in punishment"));
		guiItems.add(guiItem(playerHead(Bukkit.getPlayer("ShaneBee")), "&aSTAFF", "&7Don't annoy us", "&cDon't ask for staff", "&7Respect all staff"));
		guiItems.add(guiItem(Material.BARRIER, "&cEXIT", "&7Click to close this menu"));
		for (int i = 0; i <= 8; i++) {
			GUI.setItem(i, guiItems.get(i));
		}
		player.openInventory(GUI);
	}

	@EventHandler
	private void onRulesClick(InventoryClickEvent event) {
		if (event.getView().getTitle().equalsIgnoreCase(Utils.colString("&bServer &3Rules"))) {
			event.setCancelled(true);
			if (event.getSlot() == 8) {
				event.getView().close();
			}
		}
	}

}
