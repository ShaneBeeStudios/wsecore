package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.shanebee.wseCore.utils.Utils;

import java.util.ArrayList;

@SuppressWarnings("NullableProblems")
public class Ranks implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] strings) {
		if (sender instanceof Player) {
			openRanksGUI(((Player) sender));
		} else {
			Utils.inGameOnlyCommand();
		}
		return true;
	}

	private ItemStack guiItem(Material material, String name, String... lore) {
		return guiItem(new ItemStack(material), name, lore);
	}

	private ItemStack guiItem(ItemStack stack, String name, String... lore) {
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(Utils.colString(name));
		ArrayList<String> loreList = new ArrayList<>();
		for (String l : lore) {
			loreList.add(Utils.colString(l));
		}
		loreList.add("");
		meta.setLore(loreList);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		stack.setItemMeta(meta);
		return stack;
	}

	private void openRanksGUI(Player player) {
		Inventory GUI = Bukkit.createInventory(player, 9, Utils.colString("&3Ranks"));
		ArrayList<ItemStack> guiItems = new ArrayList<>();
		guiItems.add(guiItem(Material.GRAY_STAINED_GLASS_PANE, " ", ""));
		guiItems.add(guiItem(Material.GRAY_STAINED_GLASS_PANE, " ", ""));
		guiItems.add(guiItem(Material.CHICKEN_SPAWN_EGG, "&cNewbie", "&7This is the rank you get", "&7when you first join the game"));
		guiItems.add(guiItem(Material.PLAYER_HEAD, "&aPlayer", "&7This rank is given once you", "&7advance thru the spawn"));
		guiItems.add(guiItem(Material.DIAMOND,"&bVIP", "&7This is a purchasable rank", "&7it bypasses all of the &bWORST", "&7stuff about the server"));
		guiItems.add(guiItem(Material.EMERALD, "&5VIP+", "&c*coming soon*"));
		guiItems.add(guiItem(Material.GRAY_STAINED_GLASS_PANE, " ", ""));
		guiItems.add(guiItem(Material.GRAY_STAINED_GLASS_PANE, " ", ""));
		guiItems.add(guiItem(Material.BARRIER, "&cEXIT", "&7Click to exit"));


		for (int i = 0; i <= 8; i++) {
			GUI.setItem(i, guiItems.get(i));
		}
		player.openInventory(GUI);

	}
	@EventHandler
	private void onRanksClick(InventoryClickEvent event) {
		if (event.getView().getTitle().equalsIgnoreCase(Utils.colString("&3Ranks"))) {
			event.setCancelled(true);
			if (event.getSlot() == 8) {
				event.getView().close();
			}
		}
	}
}
