package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class WolfWhisperer implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			Player receiver;
			if (args.length == 1)
				receiver = Bukkit.getPlayer(args[0]);
			else
				receiver = player;
			if (receiver != null)
				giveWolfWhisperer(receiver);
			else
				player.sendMessage(colString("&cThe player &b" + args[0] + " &cis not online!"));

		} else {
			if (args.length == 1) {
				Player receiver = Bukkit.getPlayer(args[0]);
				if (receiver != null)
					giveWolfWhisperer(receiver);
				else
					sender.sendMessage(colString("&cThe player &b" + args[0] + " &cis not online!"));
			} else {
				sender.sendMessage("Enter a player name!");
			}
		}
		return true;
	}

	private void giveWolfWhisperer(Player player) {
		ItemStack item = new ItemStack(Material.BONE);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(colString("&aWolf Whisperer"));
		meta.setLore(Arrays.asList(colString("&eHold in your off-hand"), colString("&eat night to repel wolves"), colString("&ein the wilderness")));
		meta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);

	}

	private String colString(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

}
