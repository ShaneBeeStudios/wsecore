package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.shanebee.wseCore.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class HelpMenu implements Listener, CommandExecutor {

	private HashMap<Player, Integer> helpMenu;

	public HelpMenu() {
		this.helpMenu = new HashMap<>();
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		this.helpMenu.put(event.getPlayer(), 0);
	}

	@SuppressWarnings("NullableProblems")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			openHelpMenu(player);
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run in game!");
		}
		return true;
	}

	private void openHelpMenu(Player player) {
		Inventory GUI = Bukkit.createInventory(player, 54, Utils.colString("&3Help Menu &7- &bClick for help"));
		if (!this.helpMenu.containsKey(player)) {
			this.helpMenu.put(player, 0);
		}

		for (int i = 0; i <= 53; i++) {
			GUI.setItem(i, blankGUISlot());
		}
		int random = new Random().nextInt(53);
		GUI.setItem(random, guiItem(Material.PAPER, "&a&lCLICK FOR HELP", ""));
		player.openInventory(GUI);
	}

	private ItemStack blankGUISlot() {
		return guiItem(Material.GRAY_STAINED_GLASS_PANE, " ", "");
	}

	private ItemStack guiItem(Material material, String name, String... lore) {
		return guiItem(new ItemStack(material), name, lore);
	}

	private ItemStack guiItem(ItemStack stack, String name, String... lore) {
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(Utils.colString(name));
		ArrayList<String> loreList = new ArrayList<>();
		for (String l : lore) {
			loreList.add(Utils.colString(l));
		}
		meta.setLore(loreList);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		stack.setItemMeta(meta);
		return stack;
	}

	@EventHandler
	private void onHelpClick(InventoryClickEvent event) {
		if (event.getView().getTitle().equalsIgnoreCase(Utils.colString("&3Help Menu &7- &bClick for help"))) {
			event.setCancelled(true);
			if (event.getCurrentItem().getType() == Material.PAPER) {
				Player player = ((Player) event.getView().getPlayer());

				if (this.helpMenu.get(player) <= 10) {
					openHelpMenu(player);
					this.helpMenu.put(player, this.helpMenu.get(player) + 1);
				} else {
					event.getView().close();
					Bukkit.dispatchCommand(player, "ehelp");
				}

			}
		}
	}

}
