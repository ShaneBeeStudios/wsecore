package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

@SuppressWarnings("NullableProblems")
public class PlayerCount implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		int playerSize = 0;
		for (OfflinePlayer p : Bukkit.getOfflinePlayers()) {
			playerSize++;
		}
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&cW&7-&6S&7-&eE&7] - &6Player count: &b" + playerSize));
		return true;
	}

}
