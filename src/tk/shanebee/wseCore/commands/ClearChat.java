package tk.shanebee.wseCore.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.utils.Utils;

public class ClearChat implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			for (int i = 0; i < 100; i++) {
				player.sendMessage(" ");
			}
			player.sendMessage(Utils.colString("&7[&bChat&3Manager&7] - &6Chat cleared by &b" + sender.getName()));
		}
		return true;
	}

}
