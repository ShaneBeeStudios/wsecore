package tk.shanebee.wseCore.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.shanebee.wseCore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class VIPCardCmd implements CommandExecutor {

	@SuppressWarnings("NullableProblems")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			ItemStack item = new ItemStack(Material.PAPER);
			ItemMeta meta = item.getItemMeta();

			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&bVIP CARD"));
			List<String> lore = new ArrayList<>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&7Cash this in to get"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&7VIP rank"));
			meta.setLore(lore);
			meta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
			meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			item.setItemMeta(meta);
			player.getInventory().addItem(item);
		} else {
			Utils.inGameOnlyCommand();
		}
		return true;
	}
}
