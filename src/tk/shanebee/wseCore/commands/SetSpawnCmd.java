package tk.shanebee.wseCore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

public class SetSpawnCmd implements CommandExecutor {

	@SuppressWarnings("NullableProblems")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			WSECore.plugin.getWSEConfig().saveValue("spawn.location", player.getLocation());
			WSECore.plugin.getWSEConfig().spawnLocation = player.getLocation();
			Utils.sendPluginMessage(player, "&6Spawn has been set at your location");
		} else {
			Utils.inGameOnlyCommand();
		}
		return true;
	}

}
