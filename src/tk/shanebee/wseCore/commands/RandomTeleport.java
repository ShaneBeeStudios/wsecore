package tk.shanebee.wseCore.commands;

import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.util.HashMap;
import java.util.Random;

public class RandomTeleport implements CommandExecutor {

	private HashMap<Player, Long> times;
	private int rtpDelay;

	public RandomTeleport() {
		this.times = new HashMap<>();
		this.rtpDelay = WSECore.plugin.getWSEConfig().rtpWait;
	}

	@SuppressWarnings("NullableProblems")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if (sender instanceof Player) {
			Player player = ((Player) sender);
			if (times.containsKey(player) && !player.hasPermission("wse.admin" +
					"")) {
				if (secSinceLast(player) < ( 1000 * rtpDelay)) {
					long wait = rtpDelay - (secSinceLast(player) / 1000);
					Utils.sendPluginMessage(player, "&6You have to wait &b" + wait + " seconds &6before you can use RTP again!");
					return true;
				}
			} else {
				times.put(player, System.currentTimeMillis());
			}
			Utils.sendPluginMessage(player, "&6Looking for a safe location for you!");
			Location loc = randomLocation(player.getWorld());

			Utils.sendPluginMessage(player, "&6Teleporting you to x:&b" + loc.getX() + " &6y:&b" + loc.getY() + " &6z:&b" + loc.getZ());
			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> player.teleport(loc), 20 * 2);
		}
		return true;
	}

	private Location randomLocation(World world) {
		int max = WSECore.plugin.getWSEConfig().rtpMax;
		int x = new Random().nextInt(max * 2) - max;
		int z = new Random().nextInt(max * 2) - max;
		Location loc = new Location(world, x, 60, z);
		Biome biome = loc.getBlock().getBiome();
		switch (biome) {
			case OCEAN:
			case DEEP_OCEAN:
			case COLD_OCEAN:
			case DEEP_COLD_OCEAN:
			case DEEP_FROZEN_OCEAN:
			case DEEP_LUKEWARM_OCEAN:
			case DEEP_WARM_OCEAN:
			case FROZEN_OCEAN:
			case LUKEWARM_OCEAN:
			case WARM_OCEAN:
			case BEACH:
			case SNOWY_BEACH:
				return randomLocation(world);
			default:
				Chunk chunk = loc.getWorld().getChunkAt(loc);
				for (int i = 60; i < 250; i++) {
					loc.setY(i);
					if (loc.getBlock().getType() == Material.AIR && loc.getBlock().getRelative(BlockFace.UP).getType() == Material.AIR) {
						if (blockCheck(loc.getBlock().getRelative(BlockFace.DOWN))) {
							return loc;
						}
					}
				}
				chunk.unload();
				return randomLocation(world);
		}
	}

	private long secSinceLast(Player player) {
		long lastRTP = this.times.get(player);
		return System.currentTimeMillis() - lastRTP;
	}

	private boolean blockCheck(Block block) {
		Material type = block.getType();
		switch (type) {
			case GRASS:
			case GRASS_BLOCK:
			case GRASS_PATH:
			case MYCELIUM:
			case DIRT:
			case COARSE_DIRT:
			case PODZOL:
			case SAND:
			case COBBLESTONE:
			case MOSSY_COBBLESTONE:
				return true;
		}
		return false;
	}

}
