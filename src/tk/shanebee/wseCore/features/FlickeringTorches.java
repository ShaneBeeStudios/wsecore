package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Lightable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;
import tk.shanebee.wseCore.WSECore;

import java.util.List;
import java.util.Random;

public class FlickeringTorches implements Listener {

	private List<Location> torches;

	public FlickeringTorches(WSECore plugin) {
		startFlickeringTorches();
		this.torches = WSECore.plugin.getTorchData().getTorchLocations();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	private void startFlickeringTorches() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			for (Location loc : torches) {
				Block block = loc.getBlock();
				Material type = block.getType();
				if (type == Material.TORCH || type == Material.REDSTONE_TORCH) {
					int ran = new Random().nextInt(100) + 1;
					if (ran <= 90) continue;
					if (ran <= 91)
						setFlickerLevel(block, FlickerLevel.BRIGHT);
					else if (ran <= 95)
						setFlickerLevel(block, FlickerLevel.DIM);
					else
						setFlickerLevel(block, FlickerLevel.OFF);

				}
			}
		}, 0, 15);
	}

	private void setFlickerLevel(Block block, FlickerLevel level) {
		switch (level) {
			case BRIGHT:
				block.setType(Material.TORCH);
				break;
			case DIM:
				block.setType(Material.AIR);
				block.setType(Material.REDSTONE_TORCH);
				break;
			case OFF:
				block.setType(Material.REDSTONE_TORCH);
				Lightable light = (Lightable) block.getBlockData();
				light.setLit(false);
				block.setBlockData(light);
				break;
		}
	}

	@EventHandler
	private void onBlockUpdate(BlockPhysicsEvent event) {
		Block block = event.getBlock();
		if (block.getType() == Material.REDSTONE_TORCH) {
			if (!((Lightable) block.getBlockData()).isLit()) {
				event.setCancelled(true);
			}
		}
	}

	public enum FlickerLevel {
		BRIGHT,
		DIM,
		OFF
	}

}
