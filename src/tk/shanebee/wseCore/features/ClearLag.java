package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;

import java.util.Random;

public class ClearLag {

	public ClearLag() {
		clearLag();
	}

	private void clearLag() {
		WSECore plugin = WSECore.plugin;
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				sendMsgAllPlayers("&7[&cClearLag&7] - &6Clearing all drops in 60 seconds");
				Bukkit.getScheduler().runTaskLater(plugin, () -> sendMsgAllPlayers("&7[&cClearLag&7] - &6Clearing all drops in 30 seconds"), 20 * 30);

				Bukkit.getScheduler().runTaskLater(plugin, () -> sendMsgAllPlayers("&7[&cClearLag&7] - &6Clearing all drops in 10 seconds"), 20 * 50);

				Bukkit.getScheduler().runTaskLater(plugin, () -> sendMsgAllPlayers("&7[&cClearLag&7] - &6Clearing all drops in 3"), 20 * 60);
				Bukkit.getScheduler().runTaskLater(plugin, () -> sendMsgAllPlayers("&7[&cClearLag&7] - &6Clearing all drops in 2"), 20 * 61);
				Bukkit.getScheduler().runTaskLater(plugin, () -> sendMsgAllPlayers("&7[&cClearLag&7] - &6Clearing all drops in 1"), 20 * 62);

				int ran = new Random().nextInt(380) + 15;
				Bukkit.getScheduler().runTaskLater(plugin, () -> sendMsgAllPlayers("&7[&cClearLag&7] - &6Cleared &b" + ran + " &6drops"), 20 * 63);
			}
		}, 20 * 60 * 5, 20 * 60 * 5);
	}

	private void sendMsgAllPlayers(String message) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}

}
