package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;

public class HighAltitude {

	public HighAltitude() {
		startAltitudeTask();
	}

	private void startAltitudeTask() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, new Runnable() {
			@Override
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					Location loc = player.getLocation();
					double y = loc.getY();
					if (y < 90) continue;

					float level = 0;
					if (isBetween(y, 90, 100))
						level = 0.25f;
					else if (isBetween(y, 101, 110))
						level = 0.35f;
					else if (isBetween(y, 111, 125))
						level = 0.45f;
					else if (isBetween(y, 126, 150))
						level = 0.55f;
					else if (isBetween(y, 151, 175))
						level = 0.65f;
					else if (isBetween(y, 176, 200))
						level = 0.75f;
					else if (isBetween(y, 201, 255))
						level = 0.95f;

					player.playSound(loc, Sound.ITEM_ELYTRA_FLYING, level, 1);

				}
			}
		}, 20 * 10, 20 * 10);
	}

	private boolean isBetween(double y, double d1, double d2) {
		return y >= d1 && y <= d2;
	}

}
