package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;
import tk.shanebee.wseCore.WSECore;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TimeCycle {

	private World world;

	public TimeCycle() {
		this.world = Bukkit.getWorld("world");
		assert this.world != null;
		this.world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
		cycleTime();
	}

	private void cycleTime() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			List<Integer> times = Arrays.asList(1, 5, 7, 20, 35);
			int random = new Random().nextInt(5);
			long time = world.getTime();
			if (time % 24000 > 13000) {
				world.setTime(time + times.get(random));
			} else {
				world.setTime(time + 10);
			}
		}, 0, 5);
	}

}
