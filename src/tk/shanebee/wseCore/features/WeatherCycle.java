package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Weather;

import java.util.Random;

public class WeatherCycle {

	private World world;

	public WeatherCycle() {
		this.world = Bukkit.getWorld("world");
		assert this.world != null;
		this.world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
		weatherCycle();
	}

	private void weatherCycle() {
		Random random = new Random();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			int w = random.nextInt(10) + 1;
			if (w <= 7)
				Weather.setWeather(world, Weather.SUNNY);
			else if (w <= 9)
				Weather.setWeather(world, Weather.RAINY);
			else if (w == 10)
				Weather.setWeather(world, Weather.STORMY);
		}, 20 * 60 * 4, 20 * 60 * 4);
	}

}
