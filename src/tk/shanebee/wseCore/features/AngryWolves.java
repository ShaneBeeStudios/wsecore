package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.shanebee.wseCore.WSECore;

public class AngryWolves {

	public AngryWolves() {
		startAngryWolves();
	}

	private void startAngryWolves() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.getGameMode() == GameMode.CREATIVE || player.getGameMode() == GameMode.SPECTATOR) continue;
				if (!player.getWorld().getName().equalsIgnoreCase("world")) continue;
				boolean night = false;
				if (player.getWorld().getTime() % 24000 > 13000) night = true;
				if (hasWolfWhisperer(player)) night = false;

				for (Entity entity : player.getNearbyEntities(16, 16, 16)) {
					if (entity instanceof Wolf) {
						Wolf wolf = ((Wolf) entity);
						wolf.setAngry(night);
						wolf.setTarget(night ? player : null);
					}
				}
			}
		}, 20 * 5, 20 * 5);
	}

	private boolean hasWolfWhisperer(Player player) {
		ItemStack item = player.getInventory().getItem(EquipmentSlot.OFF_HAND);
		if (item != null && item.hasItemMeta()) {
			ItemMeta meta = item.getItemMeta();
			return meta.hasDisplayName() && meta.getDisplayName()
					.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&aWolf Whisperer"));
		}
		return false;
	}


}
