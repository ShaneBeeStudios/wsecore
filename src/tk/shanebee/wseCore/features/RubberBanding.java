package tk.shanebee.wseCore.features;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;

public class RubberBanding {

	public RubberBanding() {
		rubberBanding();
	}

	private void rubberBanding() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.hasPermission("wse.bypass")) continue;
				Location previous = player.getLocation();
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
					Location current = player.getLocation();
					double distance = current.distance(previous);
					if (distance > 7 && distance < 50) {
						player.teleport(previous);
						Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> player.teleport(previous), 5);
						Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> player.teleport(previous), 10);
					}
				}, 30);
			}
		}, 0, 600);
	}

}
