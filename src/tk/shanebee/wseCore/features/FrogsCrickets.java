package tk.shanebee.wseCore.features;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.BoundingBox;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Bound;
import tk.shanebee.wseCore.utils.Weather;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FrogsCrickets {

	public FrogsCrickets() {
		startSoundTask();
	}

	private void startSoundTask() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			for (Player player : Bukkit.getOnlinePlayers()) {
				int random = new Random().nextInt(100) + 1;
				if (random >= 15) continue;
				if (!player.getWorld().getName().equalsIgnoreCase("world")) continue;
				if (player.getWorld().getTime() % 24000 < 13000) continue;
				int x = ((int) player.getLocation().getX());
				int y = ((int) player.getLocation().getY());
				int z = ((int) player.getLocation().getZ());

				Bound bound = new Bound(player.getWorld().getName(), x + 10, y + 10, z + 10, x - 10, y - 10, z -10);
				List<Block> blocks;
				if (Weather.getWeather(player.getWorld()) == Weather.SUNNY) {
					blocks = new ArrayList<>(bound.getBlocks(Material.GRASS_BLOCK, Material.WATER));
				} else {
					blocks = new ArrayList<>(bound.getBlocks(Material.WATER));
				}

				if (!blocks.isEmpty()) {
					int ran = new Random().nextInt(blocks.size());
					Block block = blocks.get(ran);
					Location loc = block.getLocation();
					World world = player.getWorld();
					if (block.getType() == Material.GRASS_BLOCK)
						world.playSound(loc, "cricket", 0.25f, 1);
					else if (block.getType() == Material.WATER && block.getLocation().getY() > 55)
						world.playSound(loc, "frog", 0.25f, 1);
				}

			}
		}, 20, 20);
	}


}

/*
every second:
	chance of 10%:
		loop all players:
			if time in world of loop-player is between 18:00 and 23:59:
				if weather in world of loop-player is clear:
					loop blocks in radius 10 around loop-player:
						if loop-block is grass:
							add location of loop-block to {_test::*}
					set {_block} to random element of {_test::*}
					soundloc({_block}, loop-player, "cricket", 0.25)
	chance of 10%:
		loop all players:
			if time in world of loop-player is between 18:00 and 11:59:
				if y coord of loop-player > 55:
					loop blocks in radius 7 around loop-player:
						if loop-block is water:
							add location of loop-block to {_test::*}
					set {_block} to random element of {_test::*}
					soundloc({_block}, loop-player, "frog", 0.25)
			if time in world of loop-player is between 0:01 and 6:00:
				if y coord of loop-player > 55:
					loop blocks in radius 7 around loop-player:
						if loop-block is water:
							add location of loop-block to {_test::*}
					set {_block} to random element of {_test::*}
					soundloc({_block}, loop-player, "frog", 0.25)
 */
