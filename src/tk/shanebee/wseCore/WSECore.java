package tk.shanebee.wseCore;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import tk.shanebee.wseCore.commands.*;
import tk.shanebee.wseCore.data.PlayerData;
import tk.shanebee.wseCore.data.TorchData;
import tk.shanebee.wseCore.features.*;
import tk.shanebee.wseCore.hooks.ProSupport;
import tk.shanebee.wseCore.hooks.Vault;
import tk.shanebee.wseCore.listeners.*;
import tk.shanebee.wseCore.hooks.Perm;
import tk.shanebee.wseCore.utils.Utils;

public class WSECore extends JavaPlugin {

	private Config config;
	private PlayerData playerData;
	private TorchData torchData;
	public static WSECore plugin;

	private Perm perms;
	private Vault vault;
	private ProSupport psAPI = null;

	@Override
	public void onEnable() {
		Utils.log("&7Loading CORE!");
		plugin = this;
		this.config = new Config(this);
		this.playerData = new PlayerData(this);
		this.torchData = new TorchData(this);
		this.perms = new Perm(this);

		registerCommands();
		registerEvents();
		registerFeatures();
		this.vault = new Vault();
		this.vault.setupEconomy();
		this.vault.setupChat();

		Utils.log("&7CORE &aenabled!");
		if (Bukkit.getPluginManager().getPlugin("ProtocolSupport") != null) {
			this.psAPI = new ProSupport();
		}


	}

	@Override
	public void onDisable() {
	}

	@SuppressWarnings("ConstantConditions")
	private void registerCommands() {
		getCommand("setspawn").setExecutor(new SetSpawnCmd());
		getCommand("spawn").setExecutor(new SpawnCmd());
		getCommand("vipcard").setExecutor(new VIPCardCmd());
		getCommand("playercount").setExecutor(new PlayerCount());
		Rules rules = new Rules();
		getCommand("rules").setExecutor(rules);
		getServer().getPluginManager().registerEvents(rules, this);

		Ranks ranks = new Ranks();
		getCommand("ranks").setExecutor(ranks);
		getServer().getPluginManager().registerEvents(ranks, this);
		getCommand("wolfwhisperer").setExecutor(new WolfWhisperer());

		HelpMenu help = new HelpMenu();
		getServer().getPluginManager().registerEvents(help, this);
		getCommand("help").setExecutor(help);

		VoteCmd vote = new VoteCmd();
		getCommand("vote").setExecutor(vote);
		getCommand("votekey").setExecutor(vote);

		getCommand("clearchat").setExecutor(new ClearChat());
		getCommand("rtp").setExecutor(new RandomTeleport());
	}

	private void registerEvents() {
		getServer().getPluginManager().registerEvents(new JoinEvents(), this);
		getServer().getPluginManager().registerEvents(new ClickEvents(), this);
		getServer().getPluginManager().registerEvents(new BuildingEvents(), this);
		getServer().getPluginManager().registerEvents(new EntityEvents(), this);
		getServer().getPluginManager().registerEvents(new PlayerTablist(), this);
		getServer().getPluginManager().registerEvents(new Bossbar(), this);
		getServer().getPluginManager().registerEvents(new MOTD(), this);
		getServer().getPluginManager().registerEvents(new FakeOpEvents(), this);
		getServer().getPluginManager().registerEvents(new ReversePVP(), this);
		getServer().getPluginManager().registerEvents(new ColoredChat(), this);
		getServer().getPluginManager().registerEvents(new CommandHandler(), this);
		getServer().getPluginManager().registerEvents(new PluginsVersions(), this);
		getServer().getPluginManager().registerEvents(new JoinMessages(), this);
		getServer().getPluginManager().registerEvents(new BadassCreepers(), this);
		if (Bukkit.getPluginManager().getPlugin("WorldEdit") != null && Bukkit.getPluginManager().getPlugin("WorldGuard") != null)
			if (Bukkit.getPluginManager().getPlugin("WorldEdit").isEnabled() && Bukkit.getPluginManager().getPlugin("WorldGuard").isEnabled()) {
				getServer().getPluginManager().registerEvents(new SpawnLightning(), this);
				Utils.log("WorldGuard hooks enabled");
			} else {
				Utils.warn("WorldGuard hooks failed to load!");
			}
		getServer().getPluginManager().registerEvents(new CitizensClick(), this);
		getServer().getPluginManager().registerEvents(new NoGroundLeafDecay(), this);
		getServer().getPluginManager().registerEvents(new AlwaysPhantoms(), this);
		getServer().getPluginManager().registerEvents(new FallVoid(), this);
	}

	private void registerFeatures() {
		new RubberBanding();
		new TimeCycle();
		new WeatherCycle();
		new ClearLag();
		new AngryWolves();
		new FrogsCrickets();
		new HighAltitude();
		new FlickeringTorches(this);
	}

	public Config getWSEConfig() {
		return this.config;
	}

	public PlayerData getPlayerData() {
		return this.playerData;
	}

	public TorchData getTorchData() {
		return this.torchData;
	}

	public Perm getPerms() {
		return this.perms;
	}

	public Vault getVault() {
		return this.vault;
	}

	public ProSupport getProAPI() {
		return this.psAPI;
	}


}
