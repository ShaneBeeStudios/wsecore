package tk.shanebee.wseCore.hooks;

import org.bukkit.entity.Player;
import protocolsupport.api.ProtocolSupportAPI;
import protocolsupport.api.ProtocolVersion;

public class ProSupport {

	public ProSupport() {
		disableVersions();
	}

	private void disableVersions() {
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_8);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_7_10);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_7_5);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_6_4);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_6_2);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_6_1);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_5_2);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_5_1);
		ProtocolSupportAPI.disableProtocolVersion(ProtocolVersion.MINECRAFT_1_4_7);
	}

	public String getPlayerVersion(Player player) {
		ProtocolVersion ver = ProtocolSupportAPI.getProtocolVersion(player);
		return ver.getName();
	}

}
