package tk.shanebee.wseCore.hooks;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import tk.shanebee.wseCore.WSECore;

public class Perm {

	private WSECore plugin;
	private Permission manager;

	public Perm(WSECore plugin) {
		this.plugin = plugin;
		setupPermissions();
	}

	public void setGroup(Player player, String newGroup) {
		for (String group : manager.getPlayerGroups(null, player)) {
			manager.playerRemoveGroup(null, player, group);
		}
		manager.playerAddGroup(null, player, newGroup);
	}

	public boolean isInGroup(Player player, String group) {
		return manager.playerInGroup(player, group);
	}

	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> rsp = plugin.getServer().getServicesManager().getRegistration(Permission.class);
		assert rsp != null;
		manager = rsp.getProvider();
		return true;
	}

}
