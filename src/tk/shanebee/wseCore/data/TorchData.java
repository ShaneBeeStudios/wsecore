package tk.shanebee.wseCore.data;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TorchData {

	private WSECore plugin;
	private File torchFile;
	private FileConfiguration torchData;
	private List<Location> torchLocations = new ArrayList<>();
	private List<Location> lightningLocations = new ArrayList<>();

	public TorchData(WSECore plugin) {
		this.plugin = plugin;
		loadTorchData();
		loadLocations();
	}

	private void loadTorchData() {
		if (torchFile == null) {
			torchFile = new File(plugin.getDataFolder(), "torchData.yml");
		}
		if (!torchFile.exists()) {
			try {
				//noinspection ResultOfMethodCallIgnored
				torchFile.createNewFile();
			} catch (IOException e) {
				Utils.warn("Could not create torchData.yml");
			}
			torchData = YamlConfiguration.loadConfiguration(torchFile);
			saveConfig();
		} else {
			torchData = YamlConfiguration.loadConfiguration(torchFile);
		}
	}

	private void loadLocations() {
		int i = 0;
		for (String string : torchData.getStringList("torches")) {
			torchLocations.add(stringToLoc(string));
			i++;
		}
		Utils.log("&7Successfully loaded &b" + i + "&7 torches!");
		i = 0;
		for (String string : torchData.getStringList("lightning")) {
			lightningLocations.add(stringToLoc(string));
			i++;
		}
		Utils.log("&7Successfully loaded &b" + i + "&7 lightning locations!");

	}

	public List<Location> getTorchLocations() {
		return this.torchLocations;
	}

	public List<Location> getLightningLocations() {
		return this.lightningLocations;
	}


	private void saveConfig() {
		try {
			this.torchData.save(this.torchFile);
		} catch (IOException e) {
			Utils.warn("Could not save torchData.yml");
		}
	}

	public void saveValue(String path, Object value) {
		this.torchData.set(path, value);
		saveConfig();
	}

	private Location stringToLoc(String string) {
		String[] locs = string.split(":");
		return new Location(Bukkit.getWorld(locs[0]), Double.valueOf(locs[1]), Double.valueOf(locs[2]), Double.valueOf(locs[3]));
	}

	public String locToString(Location loc) {
		String w = loc.getWorld().getName();
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();
		return w + ":" + x + ":" + y + ":" + z;
	}

}
