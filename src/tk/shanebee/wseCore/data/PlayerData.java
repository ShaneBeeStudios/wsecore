package tk.shanebee.wseCore.data;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.io.File;
import java.io.IOException;

public class PlayerData {

	private WSECore plugin;

	private File playerFile;
	private FileConfiguration playerData;

	public PlayerData(WSECore plugin) {
		this.plugin = plugin;
		loadPlayerConfig();
	}

	private void loadPlayerConfig() {
		if (playerFile == null) {
			playerFile = new File(plugin.getDataFolder(), "playerData.yml");
		}
		if (!playerFile.exists()) {
			try {
				//noinspection ResultOfMethodCallIgnored
				playerFile.createNewFile();
			} catch (IOException e) {
				Utils.warn("&eCould not create playerData.yml");
			}
			playerData = YamlConfiguration.loadConfiguration(playerFile);
			saveConfig();

		} else {
			playerData = YamlConfiguration.loadConfiguration(playerFile);
		}
	}

	private void saveConfig() {
		try {
			this.playerData.save(this.playerFile);
		} catch (IOException e) {
			Utils.warn("Could not save playerData.yml");
		}
	}


	public void savePlayerData(Player player, PlayerDataType type, Object value) {
		playerData.set("players." + player.getUniqueId() + "." + type.getType(), value);
		saveConfig();
	}

	public Object getPlayerData(Player player, PlayerDataType type) {
		Object object;
		try {
			object = playerData.get("players." + player.getUniqueId() + "." + type.getType());
		} catch (Exception ignore) {
			object = null;
		}
		return object;
	}

	public FileConfiguration getPlayerDataConfig() {
		return this.playerData;
	}


	public enum PlayerDataType {
		PLAYER_NAME("name"),
		PLAYER_IP("ip"),
		PLAYER_LAST_VOTE("last-vote"),
		PLAYER_TOTAL_VOTES("total-votes");

		String type;

		PlayerDataType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}
}
