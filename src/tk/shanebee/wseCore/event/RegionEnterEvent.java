package tk.shanebee.wseCore.event;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RegionEnterEvent extends Event {

	private static HandlerList handlers = new HandlerList();
	private ProtectedRegion region;
	private Player player;

	public RegionEnterEvent(ProtectedRegion region, Player player) {
		this.region = region;
		this.player = player;
	}

	public Player getPlayer() {
		return this.player;
	}

	public ProtectedRegion getRegion() {
		return this.region;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
