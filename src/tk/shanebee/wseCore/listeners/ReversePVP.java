package tk.shanebee.wseCore.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class ReversePVP implements Listener {

	@EventHandler
	private void onPVP(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player damager = ((Player) event.getDamager());
			event.setCancelled(true);
			damager.damage(event.getDamage());
		}
	}

}
