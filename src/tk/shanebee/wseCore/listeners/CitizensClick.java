package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import tk.shanebee.wseCore.utils.Utils;

public class CitizensClick implements Listener {

	@EventHandler
	private void onClickCitizen(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		Player player = event.getPlayer();
		if (event.getHand() == EquipmentSlot.OFF_HAND) return;
		String name = entity.getCustomName();
		assert name != null;
		if (name.equalsIgnoreCase(Utils.colString("&7[&aSTART&7]"))) {
			Utils.scm(player, "&7[&aSTART&7] - &eeeeeeeeee that tickles ... click the start sign not me!");
		} else if (name.equalsIgnoreCase(Utils.colString("&7[&bHELP&7]"))) {
			Bukkit.dispatchCommand(player, "help");
		} else if (name.equalsIgnoreCase(Utils.colString("&7[&dRTP&7]"))) {
			Bukkit.dispatchCommand(player, "rtp");
		} else if (name.equalsIgnoreCase(Utils.colString("&7[&bVote&3Helper&7]"))) {
			Utils.scm(player, "&7[&bVOTES&7] - &bTo vote... type /vote!");
		}
	}

}
