package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.util.Random;

public class EntityEvents implements Listener {

	@EventHandler
	private void onZombieDeath(EntityDeathEvent event) {
		if (event.getEntity().getType() == EntityType.ZOMBIE) {
			LivingEntity entity = event.getEntity();
			if (entity.getKiller() != null) {
				Player player = entity.getKiller();
				Location loc = event.getEntity().getLocation();
				int random = new Random().nextInt(100) + 1;

				if (random <= 15) {
					Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
						loc.getWorld().spawnEntity(loc, EntityType.ZOMBIE);
						Utils.scm(player, "&7[&cRESURRECTION&7] - &eUH OH... HE'S BAAAACCKKKK!!!");
					}, 60);
				}
			}
		}
	}

	@EventHandler
	private void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		Location loc = player.getLocation();
		int x = ((int) loc.getX());
		int y = ((int) loc.getY());
		int z = ((int) loc.getZ());
		String locS = "&bx: " + x + " y: " + y + " z: " + z;

		Utils.scm(player, "&7----------&cYOU DIED&7----------");
		Utils.scm(player, "&6Location: " + locS);
		Utils.scm(player, "&6World: &b" + loc.getWorld().getName());
		Utils.scm(player, "&7---------------------------");
		event.setDeathMessage(ChatColor.translateAlternateColorCodes('&', "&d" + event.getDeathMessage()));
	}

}
