package tk.shanebee.wseCore.listeners;

import org.bukkit.*;
import org.bukkit.block.Furnace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceBurnEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.util.Arrays;
import java.util.Objects;

public class FakeOpEvents implements Listener {

	@EventHandler
	private void onAskForOp(AsyncPlayerChatEvent event) {
		if (event.getMessage().contains(" have ") && event.getMessage().contains(" op")) {
			Player player = event.getPlayer();
			ItemStack item = new ItemStack(Material.COAL);
			ItemMeta meta = item.getItemMeta();

			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&bOP"));
			meta.setLore(Arrays.asList(ChatColor.RED + "**EXTREMELY DANGEROUS**", ChatColor.GRAY + "This is your very own OP",
					ChatColor.GRAY + "Use it wisely"));
			item.setItemMeta(meta);
			player.getInventory().addItem(item);

			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> Utils.sendPluginMessage(player, "&aSure.. Here... have OP!!!"), 20);
			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> Utils.scm(player,
					"&7&o[Server: Made " + player.getName() + " a server operator]"), 40);
		}
	}

	@EventHandler
	private void onUseFakeOp(FurnaceBurnEvent event) {
		Furnace furnace = ((Furnace) event.getBlock().getState());
		ItemStack fuel = furnace.getInventory().getFuel();
		assert fuel != null;
		if (fuel.getItemMeta() != null) {
			ItemMeta meta = fuel.getItemMeta();
			if (meta.hasLore()) {
				if (Objects.requireNonNull(meta.getLore()).contains(ChatColor.RED + "**EXTREMELY DANGEROUS**")) {
					Location loc = event.getBlock().getLocation();
					Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> loc.getWorld().playSound(loc, Sound.EVENT_RAID_HORN, 200, 1), 40);
					Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> loc.getWorld().strikeLightning(loc), 140);
					Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> loc.getWorld().createExplosion(loc, 4F), 142);

				}
			}
		}
	}

}
