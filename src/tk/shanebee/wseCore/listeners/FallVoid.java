package tk.shanebee.wseCore.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

public class FallVoid implements Listener {

	@EventHandler
	private void fallIntoVoid(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = ((Player) event.getEntity());
			if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
				event.setCancelled(true);
				Location spawn = WSECore.plugin.getWSEConfig().spawnLocation;
				player.teleport(spawn);
				Utils.sendPluginMessage(player, "&6Oops! Looks like you almost fell out of the world!");
				Utils.scm(player, "&6Don't worry, we saved you!");
			}
		}
	}
}
