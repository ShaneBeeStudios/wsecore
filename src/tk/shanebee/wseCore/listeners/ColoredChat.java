package tk.shanebee.wseCore.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.shanebee.wseCore.WSECore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ColoredChat implements Listener {

	private List<String> chatColors;
	private List<String> rainbowColors;
	private HashMap<Player, Location> locations = new HashMap<>();

	public ColoredChat() {
		this.chatColors = Arrays.asList("&a", "&b", "&c", "&e", "&2", "&3", "&4", "&5", "&6", "&7");
		this.rainbowColors = Arrays.asList("&4", "&c", "&6", "&e", "&2", "&a", "&b", "&9", "&d", "&5");
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		locations.put(event.getPlayer(), event.getPlayer().getLocation());
	}

	@EventHandler
	private void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		double x = player.getLocation().getX();
		double z = player.getLocation().getZ();
		double previousX = 0;
		double previousZ = 0;
		if (locations.containsKey(player)) {
			previousX = locations.get(player).getX();
			previousZ = locations.get(player).getZ();
		}

		if (!locations.containsKey(player) || (x != previousX && z != previousZ)) {
			String prefix = ChatColor.translateAlternateColorCodes('&', "&cW&7-&6S&7-&eE");
			String playerPrefix = WSECore.plugin.getVault().getPrefix(player);

			int ran = new Random().nextInt(100);
			if (ran <= 5) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"&7[&bChat&3Manager&7] - &cERROR... ERROR... it seems your message wasn't sent!"));
				return;
			}
			if (ran <= 70)
				event.setFormat(ChatColor.translateAlternateColorCodes('&',
						"&7[" + prefix + "&7]-[" + playerPrefix + "&7]-&b" + player.getName() + " &7>> " + formattedChat(event.getMessage())));
			else
				event.setFormat(ChatColor.translateAlternateColorCodes('&',
						"&7[" + prefix + "&7]-[" + playerPrefix + "&7]-&b" + player.getName() + " &7>> " + rainbowWords(event.getMessage())));
		} else {
			event.setCancelled(true);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&7[&bChat&3Manager&7] - &cYou need to move before you can chat"));
		}
	}

	private String formattedChat(String message) {
		int random = new Random().nextInt(10);
		String randomCol = this.chatColors.get(random);
		return randomCol + message;
	}

	private String rainbowWords(String message) {
		List<String> msg = Arrays.asList(message.split(" "));
		StringBuilder newString = new StringBuilder();
		int amt = 0;
		for (String string : msg) {
			newString.append(rainbowColors.get(amt)).append(string).append(" ");
			if (amt < 9) {
				amt++;
			} else {
				amt = 0;
			}
		}
		return newString.toString();
	}

}
