package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.shanebee.wseCore.WSECore;

import java.util.Random;

public class CommandHandler implements Listener {

	@EventHandler
	private void onOpCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String cmd = event.getMessage();
		if (!player.hasPermission("wse.admin")) {
			if (cmd.startsWith("/op")) {
				event.setCancelled(true);
				player.kickPlayer("Lost connection : Internal Exception : Java.oi.IOExpection : An existing Connection was forcibly closed by the remote host.");
			}

			if (cmd.startsWith("/minecraft:") || cmd.startsWith("/spigot:") || cmd.startsWith("/bukkit:")) {
				event.setCancelled(true);
				for (int i = 0; i < 100; i++) {
					player.sendMessage(" ");
				}
			}

			if (cmd.startsWith("/ver") || cmd.startsWith("/about") || cmd.startsWith("/?")) {
				event.setCancelled(true);
				player.sendMessage("This server is running CraftBukkit version git-Spigot-429b0e3-4b91e62 (MC: 1.12.2) (Implementing API version 1.12.2-R0.1-SNAPSHOT)");
				player.sendMessage("You are 519 version(s) behind");
			}
			if (cmd.startsWith("/fly")) {
				event.setCancelled(true);
				sendMsg(player, "&6Set fly mode &cenabled &6for " + player.getName());
				player.setAllowFlight(true);
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> sendMsg(player, "&6Just kidding!"), 20 * 10);
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> player.setAllowFlight(false), 20 * 11);
			}
			if (cmd.startsWith("/tps")) {
				event.setCancelled(true);
				Random random = new Random();
				String tp1 = "" + (random.nextInt(4) + 16) + "." + random.nextInt(10);
				String tp2 = "" + (random.nextInt(4) + 16) + "." + random.nextInt(10);
				String tp3 = "" + (random.nextInt(4) + 16) + "." + random.nextInt(10);
				sendMsg(player, "&6TPS from last 1m, 5m, 15m: &a*" + tp1 + ", *" + tp2 + ", *" + tp3);
			}
		}
		if (cmd.startsWith("/butcher")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "Dont use that... its bad... kills NPCs");
		}
	}

	private void sendMsg(Player player, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	@EventHandler
	private void onJoinCancelFlight(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("wse.admin")) {
			player.setAllowFlight(false);
		}
	}

}
