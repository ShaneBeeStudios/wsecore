package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.utils.Utils;

import java.util.Arrays;
import java.util.Random;

public class JoinMessages implements Listener {

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (player.hasPlayedBefore()) {
			logJoinToConsole(player, true);
			if (!player.hasPermission("wse.bypass")) {
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
					Utils.scm(player, "&6Welcome back &b" + player.getName());
					Utils.scm(player, "&a" + player.getName() + " joined the game");
				}, 20);
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> Utils.scm(player, "&7[&a+&7] &3" + player.getName()), 30);
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> Utils.scm(player, "&7[&3Server&bName&7] - &7" +
						player.getName() + " &9joined the server"), 40);
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () ->
						player.sendTitle(ChatColor.translateAlternateColorCodes('&',
								"&aWrost&7-&2Server&7-&aEver"), ChatColor.translateAlternateColorCodes('&',
								"&bWelcome back " + player.getName()), 10, 80, 20), 100);
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> player.sendTitle(ChatColor.translateAlternateColorCodes('&',
						"&aWe Hope You"), ChatColor.translateAlternateColorCodes('&',
						"&bHave the worst time ever!"), 10, 80, 20), 180);
			}
		} else {
			logJoinToConsole(player, false);
			int playerSize = 0;
			for (OfflinePlayer p : Bukkit.getOfflinePlayers()) {
				playerSize++;
			}

			for (Player p : Bukkit.getOnlinePlayers()) {
				Utils.scm(p, "&7[&3W-&bS&7-&3E&7] - &7" + player.getName() +
						" &9has joined the server for their first time &7[&b#%"  + playerSize + "%&7]");
			}
			Utils.scm(player, "&6Welcome to the server &b" + player.getName());
			Utils.scm(player, "&7[&a+&7] &3" + player.getName());
			Utils.scm(player, "&7[&3Server&bName&7] - &7" + player.getName() + " &9joined the server");
			player.sendTitle(ChatColor.translateAlternateColorCodes('&', "&aWrost&7-&2Server&7-&aEver"),
					ChatColor.translateAlternateColorCodes('&', "&bWelcome " + player.getName()), 10, 80, 20);
			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> player.sendTitle("&aWe Hope You", "&bHave the worst time ever!", 10, 80, 20), 80);
		}
	}

	@EventHandler
	private void onConnect(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("wse.bypass")) {
			int ran = new Random().nextInt(100);
			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
				if (ran < 5) {
					player.kickPlayer("Lost connection : Internal Exception : Java.oi.IOExpection : An existing Connection was forcibly closed by the remote host.");
				} else if (ran < 10) {
					player.kickPlayer(ChatColor.translateAlternateColorCodes('&',
							"&bOops&f... &cseems you lost your connection &f... &3¯\\_(ツ)_/¯"));
				}
			}, -1);

		}
	}

	private void logJoinToConsole(Player player, boolean hasPlayed) {
		String ver = "&7[&cUnavailable Version&7]";
		if (WSECore.plugin.getProAPI() != null) {
			ver = WSECore.plugin.getProAPI().getPlayerVersion(player);
		}
		String name = player.getName();
		int total = Arrays.asList(Bukkit.getOfflinePlayers()).size();
		if (hasPlayed) {
			Utils.sendColConsole("&7[&bJOIN&7-&bVER&7] - &a" + name + " &6joined using &a" + ver);
		} else {
			Utils.sendColConsole("&7[&bJOIN&7-&bVER&7] - &a" + name + "&6joined using &a" + ver + " &6for their first time &7[&b#" + total + "&7]" );
		}
	}

}
