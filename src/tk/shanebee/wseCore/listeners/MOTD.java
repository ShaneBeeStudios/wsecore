package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerListPingEvent;
import tk.shanebee.wseCore.data.PlayerData;
import tk.shanebee.wseCore.WSECore;

public class MOTD implements Listener {

	private PlayerData playerData;

	public MOTD() {
		this.playerData = WSECore.plugin.getPlayerData();
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		playerData.savePlayerData(player, PlayerData.PlayerDataType.PLAYER_NAME, player.getName());
		playerData.savePlayerData(player, PlayerData.PlayerDataType.PLAYER_IP, player.getAddress().getAddress().toString().replace("/", ""));
	}

	@EventHandler
	private void onServerListPing(ServerListPingEvent event) {
		String ip = event.getAddress().toString().replace("/", "");
		for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
			String pIP = playerData.getPlayerDataConfig().getString("players." + player.getUniqueId().toString() + ".ip");
			if (pIP != null && pIP.equalsIgnoreCase(ip)) {
				event.setMotd(ChatColor.translateAlternateColorCodes('&', "             &3Worst Server Ever\n" +
						"          &4Welcome &6back &e" + player.getName()));
			} else {
				event.setMotd(ChatColor.translateAlternateColorCodes('&', "             &3Worst Server Ever\n" +
						"          &4Welcome &6New &ePlayer!"));
			}
		}
	}

}
