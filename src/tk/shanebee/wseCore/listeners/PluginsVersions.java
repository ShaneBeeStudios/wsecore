package tk.shanebee.wseCore.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PluginsVersions implements Listener {

	private List<String> plugins;

	public PluginsVersions() {
		plugins = Arrays.asList("&aWorldEdit", "&aEssentialsX", "&cFeatherBoard", "&aCitizens", "&cEpicWorldGenerator", "&cProtocolSupport", "&cProtocolLib",
				"&aCustomCommands", "&aJobsReborn", "&aWorldGuard", "&cTabList", "&aVault", "&aEssentialsChat", "&aEssentialsChat", "&cHolographicDisplays",
				"&aAAC", "&aViaVersion", "&aViaBackwards", "&cViaRewind", "&aViaFastForward", "&aViaSideWays", "&cViaUpsideDown", "&aWorldBorder", "&aPlaceholderAPI", "&cChatControl",
				"&aPermissionsEX", "&aAuthMeReloaded", "&cTimeIsMoney", "&cNameTagEdit", "&aTitleManager", "&aSimpleScoreboards", "&cFAWE", "&aNuVotifier",
				"&aDynMap", "&aAnimatedFrames", "&aCrazyEnchantments", "&aUltimateKits", "&cVotifier", "&aVotingPluing", "&aCratesReloaded", "&aAreaShop",
				"&aGriefPrevention", "&cGriefPreventionFlags", "&aTrails", "&aMyPet", "&aSlimeFun", "&aExoticGarden", "&cQuests", "&aChestCommands", "&aBossShop",
				"&aSpigotLib", "&aSuperTrails", "&cPlugMan", "&aShopChest", "&cChestShop", "&aRankUP", "&aAutoRestart", "&aWorldGuardExtraFlags", "&cBossBarAPI",
				"&cMvdwPlaceholders", "&aAuctionHouse", "&aRandomTP", "&aExtraHardMode", "&aTitleMOTD", "&aCustomSkins", "&cSilkSpawners", "&aCrazyAuctions",
				"&aBuyCraft", "&aCitizensCMD", "&aMultiVerseCore", "&cMultiVersePortals", "&aTerrainControl", "&cOpenWorldGenerator", "&aPVPManager",
				"&cAntiPvp", "&aAdminChat", "&aHackControl", "&aAntiKillAura", "&aEpicSpawners", "&cEpicFurnaces", "&cEpicHoppers", "&aEpicToast",
				"&aSkinRestorer", "&c1.8PVP", "&aTimeIsMoney", "&aDeluxTags", "&aPickupMoney", "&cWorldGuardExtraFlags", "&aMythicMobs", "&aBossBarAPI", "&cBetterChairs");
	}

	@EventHandler
	private void onPluginCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();

		if (event.getMessage().startsWith("/pl") && !player.hasPermission("wse.admin")) {
			event.setCancelled(true);
			StringBuilder sb = new StringBuilder();
			for (String pl : plugins) {
				sb.append(pl).append("&r, ");
			}
			String newPL = sb.toString();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"Plugins (" + plugins.size() + "): " + newPL.substring(0, newPL.length() - 2)));
		}
	}

	@EventHandler
	private void onPluginTabComplete(TabCompleteEvent event) {
		if (!(event.getSender() instanceof Player)) return;
		Player player = ((Player) event.getSender());
		if (!player.hasPermission("wse.admin")) {
			String cmd = event.getBuffer();

			List<String> plugins = new ArrayList<>();
			for (String pl : this.plugins) {
				plugins.add(pl.replace("&a", "").replace("&c", ""));
			}

			if (cmd.startsWith("/ver") || cmd.startsWith("/about")) {
				event.setCompletions(plugins);
			}
		}
	}

}
