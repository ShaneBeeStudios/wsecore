package tk.shanebee.wseCore.listeners;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

import java.util.Random;

public class BadassCreepers implements Listener {

	@EventHandler
	private void onSpawnCreeper(EntitySpawnEvent event) {
		if (event.getEntity().getType() == EntityType.CREEPER) {
			int random = new Random().nextInt(15);
			if (random == 1) {
				Creeper creeper = ((Creeper) event.getEntity());
				creeper.setPowered(true);
			}
		}
	}

	@EventHandler
	private void attackCreeper(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && event.getEntity() instanceof Creeper) {
			Creeper creeper = (Creeper) event.getEntity();
			if (creeper.isPowered()) {
				creeper.explode();
			}
		}
	}

}
