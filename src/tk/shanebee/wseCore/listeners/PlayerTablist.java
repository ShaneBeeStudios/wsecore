package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.scoreboard.Scoreboard;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.data.PlayerData.PlayerDataType;
import tk.shanebee.wseCore.utils.Time;
import tk.shanebee.wseCore.utils.Utils;

import java.util.*;

public class PlayerTablist implements Listener {

	private HashMap<UUID, Integer> tasks = new HashMap<>();
	private List<String> colors = Arrays.asList("&4", "&c", "&6", "&e", "&2", "&a", "&b", "&9", "&d", "&5");

	@EventHandler
	private void onLoginSetupTablist(PlayerJoinEvent event) {
		Random random = new Random();
		Player player = event.getPlayer();
		int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, new tablistTask(player, random, colors), 0, 60);
		tasks.put(player.getUniqueId(), task);
	}

	@EventHandler
	private void onReload(ServerLoadEvent event) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			Random random = new Random();
			int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, new tablistTask(player, random, colors), 0, 60);
			tasks.put(player.getUniqueId(), task);
		}
	}

	@EventHandler
	private void onPlayerLogout(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (tasks.containsKey(player.getUniqueId())) {
			int task = tasks.get(player.getUniqueId());
			Bukkit.getScheduler().cancelTask(task);
		}
		Scoreboard score = WSECore.plugin.getServer().getScoreboardManager().getMainScoreboard();
		if (score.getTeam(player.getName()) != null) {
			//noinspection ConstantConditions
			score.getTeam(player.getName()).unregister();
		}
	}

	public class tablistTask implements Runnable {
		List<String> colors;
		Random random;
		Player player;

		tablistTask(Player player, Random random, List<String> colors) {
			this.player = player;
			this.colors = colors;
			this.random = random;
		}

		@SuppressWarnings({"ConstantConditions", "deprecation"})
		@Override
		public void run() {
			String weather;
			String color = colors.get(random.nextInt(10));
			int bal = ((int) WSECore.plugin.getVault().economy.getBalance(player));
			if (player.getWorld().hasStorm()) {
				if (player.getWorld().isThundering()) {
					weather = "Thunderbolts and Lighting";
				} else {
					weather = "Cloudy with a chance of Rain";
				}
			} else {
				weather = "Clear Skies";
			}
			String votes = "0";
			if (WSECore.plugin.getPlayerData().getPlayerData(player, PlayerDataType.PLAYER_TOTAL_VOTES) != null) {
				votes = WSECore.plugin.getPlayerData().getPlayerData(player, PlayerDataType.PLAYER_TOTAL_VOTES).toString();
			}
			String time = Time.getTimeName(player.getWorld());
			String header = "\n" + color + "--------------------\n&bWorst&7-&3Server&7-&bEver\n" + color + "--------------------";
			String footer = "\n&6IP: &7play.Worst-Server-Ever.tk:25568\n&6World: &b" + player.getWorld().getName() + " &6Time: &b" + time +
					"\n&6Weather: &b" + weather + "\n&6Bal: &b$" + bal + " &6Votes: &b" + votes;
			player.setPlayerListHeader(ChatColor.translateAlternateColorCodes('&', header));
			player.setPlayerListFooter(ChatColor.translateAlternateColorCodes('&', footer));
			String prefix = "&7[" + WSECore.plugin.getVault().getPrefix(player) + "&7] ";


			Scoreboard score = WSECore.plugin.getServer().getScoreboardManager().getMainScoreboard();
			if (score.getTeam(player.getName()) == null) {
				score.registerNewTeam(player.getName());
				score.getTeam(player.getName()).addPlayer(player);
			}
			score.getTeam(player.getName()).setPrefix(Utils.colString(prefix));
			score.getTeam(player.getName()).setSuffix(getPing(player));
		}
	}

	private String getPing(Player player) {
		int ping = ((CraftPlayer) player).getHandle().ping;
		if (ping > 250)
			return Utils.colString("&c " + ping);
		else if (ping > 125)
			return Utils.colString("&e " + ping);
		else
			return Utils.colString("&a " + ping);
	}

}
