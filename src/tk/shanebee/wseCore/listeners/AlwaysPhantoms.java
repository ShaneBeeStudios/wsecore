package tk.shanebee.wseCore.listeners;

import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedLeaveEvent;

public class AlwaysPhantoms implements Listener {

	@EventHandler
	private void onLeaveBed(PlayerBedLeaveEvent event) {
		Player player = event.getPlayer();
		player.setStatistic(Statistic.TIME_SINCE_REST, 24000 * 3);
	}

}
