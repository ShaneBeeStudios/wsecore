package tk.shanebee.wseCore.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.Random;

public class BuildingEvents implements Listener {

	@EventHandler
	private void onBreakBlock(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("wse.bypass")) {
			int random = new Random().nextInt(100) + 1;
			if (random <= 15) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	private void onPlaceBlock(BlockPlaceEvent event) {
		Block block = event.getBlockPlaced();
		if (block.getType() == Material.DIRT || block.getType() == Material.GRASS || block.getType() == Material.COBBLESTONE) {
			if (block.getRelative(BlockFace.DOWN).getType() == Material.AIR || !block.getRelative(BlockFace.DOWN).getType().isSolid()) {
				Location loc = event.getBlock().getLocation();
				loc.add(0.5, 0.0, 0.5);
				BlockData blockData = block.getBlockData();
				block.setType(Material.AIR);
				loc.getWorld().spawnFallingBlock(loc, blockData);
			}
		}
	}

}
