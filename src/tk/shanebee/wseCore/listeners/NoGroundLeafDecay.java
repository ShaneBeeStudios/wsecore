package tk.shanebee.wseCore.listeners;

import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Leaves;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;

public class NoGroundLeafDecay implements Listener {

	@EventHandler
	public void onDecay(LeavesDecayEvent event) {
		Block block = event.getBlock();
		Material below = block.getRelative(BlockFace.DOWN).getType();
		Material twoBelow = block.getRelative(BlockFace.DOWN, 2).getType();

		if (below == Material.GRASS_BLOCK || below== Material.DIRT || below == Material.PODZOL || below == Material.COARSE_DIRT) {

			event.setCancelled(true);
			Leaves leaf = (Leaves) block.getState().getBlockData();
			leaf.setPersistent(true);
			block.setBlockData(leaf);
		}
		else if (twoBelow == Material.GRASS_BLOCK || twoBelow == Material.DIRT || twoBelow == Material.PODZOL || twoBelow == Material.COARSE_DIRT) {

			if (Tag.LEAVES.isTagged(below)) {
				event.setCancelled(true);
				Leaves leaf = (Leaves) block.getState().getBlockData();
				leaf.setPersistent(true);
				block.setBlockData(leaf);
			}
		}
	}

}
