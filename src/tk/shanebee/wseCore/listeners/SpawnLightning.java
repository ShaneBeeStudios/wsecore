package tk.shanebee.wseCore.listeners;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.internal.platform.WorldGuardPlatform;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.event.RegionEnterEvent;

import java.util.List;

public class SpawnLightning implements Listener {

	/* used to save lightning locations - keep for future use
	@EventHandler
	private void onSetLightning(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getHand() == EquipmentSlot.OFF_HAND) return;
			WSECore.plugin.getTorchData().getLightningLocations().add(event.getClickedBlock().getLocation());
			List<String> locList = new ArrayList<>();
			for (Location loc : WSECore.plugin.getTorchData().getLightningLocations()) {
				locList.add(WSECore.plugin.getTorchData().locToString(loc));
			}
			WSECore.plugin.getTorchData().saveValue("lightning", locList);

			event.getPlayer().sendMessage("SAved location");
		}
	}
	 */

	private void spawnLightning() {
		List<Location> locs = WSECore.plugin.getTorchData().getLightningLocations();
		int i = 3;
		for (Location loc : locs) {
			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
				loc.getWorld().strikeLightningEffect(loc);
				spawnParticles(loc);
			}, i);
			i = i + 3;
		}
	}

	private void spawnParticles(Location loc) {
		for (int i = 5; i < 25; i = i + 5) {
			Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> loc.getWorld().spawnParticle(Particle.LAVA, loc, 100), i);
		}
	}

	@EventHandler
	private void onRegionEnter(RegionEnterEvent event) {
		ProtectedRegion region = event.getRegion();
		if (region.getId().contains("lightning")) {
			spawnLightning();
		}
	}

	@EventHandler
	private void PlayerMoveThrowRegionEvent(PlayerMoveEvent event) {
		Location from = event.getFrom();
		Location to = event.getTo();
		WorldGuardPlatform platform = WorldGuard.getInstance().getPlatform();
		RegionManager managerFROM = platform.getRegionContainer().get(BukkitAdapter.adapt(from.getWorld()));
		RegionManager managerTO = platform.getRegionContainer().get(BukkitAdapter.adapt(to.getWorld()));
		assert managerFROM != null;
		ApplicableRegionSet applicableFROM = managerFROM.getApplicableRegions(BukkitAdapter.asBlockVector(from));
		assert managerTO != null;
		ApplicableRegionSet applicableTO = managerTO.getApplicableRegions(BukkitAdapter.asBlockVector(to));
		for (ProtectedRegion region : applicableTO.getRegions()) {
			if (!applicableFROM.getRegions().contains(region)) {
				Bukkit.getPluginManager().callEvent(new RegionEnterEvent(region, event.getPlayer()));
			}
		}
	}

}
