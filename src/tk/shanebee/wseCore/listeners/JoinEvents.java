package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.shanebee.wseCore.WSECore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JoinEvents implements Listener {

	private List<String> randomMessages;

	public JoinEvents() {
		this.randomMessages = setRandomMessages();
	}

	private List<String> setRandomMessages() {
		List<String> messages = new ArrayList<>();
		messages.add("Welcome back <player>");
		messages.add("Hey <player> long time no see!");
		messages.add("WB <player>");
		messages.add("Woohoo <player> is back, let's get this party started");
		messages.add("Hey <player> how are ya?");
		messages.add("Hey <player> Welcome back!");
		messages.add("YAY <player>'s back!'");
		messages.add("Hey Welcome back <player>");
		return messages;
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (player.hasPlayedBefore()) {
			Random random = new Random();
			for (Player sender : Bukkit.getOnlinePlayers()) {
				if (sender == player) continue;
				Bukkit.getScheduler().runTaskLater(WSECore.plugin, () -> {
					int ran = random.nextInt(randomMessages.size());
					String msg = randomMessages.get(ran).replace("<player>", player.getName());
					sender.chat(msg);
				}, 20);
			}
		}
	}

}
