package tk.shanebee.wseCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.shanebee.wseCore.WSECore;

import java.util.Random;

public class Bossbar implements Listener {

	private BossBar bar;

	public Bossbar() {
		this.bar = Bukkit.createBossBar("Default message - change in config", BarColor.GREEN, BarStyle.SEGMENTED_20);
		updateBossbar();
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		this.bar.addPlayer(event.getPlayer());
	}

	private void updateBossbar() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WSECore.plugin, () -> {
			double random = new Random().nextDouble();
			bar.setProgress(random);
		}, 1, 100);
	}

}
