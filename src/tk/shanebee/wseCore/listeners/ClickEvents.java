package tk.shanebee.wseCore.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.shanebee.wseCore.WSECore;
import tk.shanebee.wseCore.hooks.Perm;
import tk.shanebee.wseCore.utils.Utils;

import java.util.Collections;

public class ClickEvents implements Listener {

	private Perm perm;

	public ClickEvents() {
		this.perm = WSECore.plugin.getPerms();
	}

	// Player clicking sign event
	@EventHandler
	private void onClickSign(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getHand() == EquipmentSlot.OFF_HAND) return;
			Player player = event.getPlayer();
			Block block = event.getClickedBlock();
			assert block != null;
			if (Tag.SIGNS.isTagged(block.getType())) {
				Sign sign = ((Sign) block.getState());
				String line1 = sign.getLine(0);
				if (line1.equalsIgnoreCase("[START]")) {
					if (perm.isInGroup(player, "default")) {
						perm.setGroup(player, "player");
						Utils.sendPluginMessage(player, "&6Congrats, you have advanced to player");
						Utils.sendPluginMessage(player, "&6You can now use the RTP sign, warp/spawn/home commands, vote commands, claim system, etc.");
						Utils.sendPluginMessage(player, "&bHAVE FUN!");
						player.getInventory().addItem(claimTool());
						player.performCommand("/kit starter");
					} else {
						Utils.sendPluginMessage(player, "&cYou have already advanced to player, no need to start again!");
					}
				} else if (line1.equalsIgnoreCase("[RTP]")) {
					player.performCommand("/rtp");
				} else if (line1.equalsIgnoreCase("[HELP]")) {
					player.performCommand("help");
				} else if (line1.equalsIgnoreCase("[WARP]")) {
					player.performCommand("/warp");
				} else if (line1.equalsIgnoreCase("[RULES]")) {
					player.performCommand("/rules");
				}
			}
		}
	}

	private ItemStack claimTool() {
		ItemStack item = new ItemStack(Material.GOLDEN_SHOVEL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Utils.colString("&bClaim&3Tool"));
		meta.setLore(Collections.singletonList(Utils.colString("&7Use this to claim land")));
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		return item;
	}

	// Player using VIP card
	@EventHandler
	private void onClickVIPCard(PlayerInteractEvent event) {
		if (event.getHand() == EquipmentSlot.OFF_HAND) return;
		Player player = event.getPlayer();
		ItemStack hand = player.getInventory().getItemInMainHand();
		if (hand.getType() == Material.PAPER) {
			if (!hand.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&bVIP CARD")))
				return;
			if (!hand.containsEnchantment(Enchantment.ARROW_INFINITE)) return;
			if (perm.isInGroup(player, "player")) {
				player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
				Utils.sendPluginMessage(player, "&6Congrats you are now a &bVIP");
				perm.setGroup(player, "VIP");
			} else {
				Utils.sendPluginMessage(player, "&cYou are already &bVIP &cor above!!");
			}
		}
	}

	// Player puts out fire
	@EventHandler
	private void onPutoutFire(PlayerInteractEvent event) {
		if (event.getAction() != Action.LEFT_CLICK_BLOCK) return;
		Player player = event.getPlayer();
		Block block = event.getClickedBlock().getRelative(event.getBlockFace());
		if (block.getType() == Material.FIRE) {
			player.setFireTicks(60);
		}
	}

}
